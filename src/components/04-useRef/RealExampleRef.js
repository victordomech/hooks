import React, { useState } from 'react';

import {MultipleCustomHooks} from '../03-examples/MultipleCustomHooks';
import '../02-useEffect/effects.css';

export const RealExampleRef = () => {

    const [show, setsShow] = useState(false)
    return (
        <div>
            <h1>Real example Ref</h1>
            <hr />

            {show && <MultipleCustomHooks /> }

            <button 
            className="btn btn-danger mt-2"
            onClick={ () => {

                setsShow(!show);
            }}
            >Show/hide</button>
        </div>
    )
}
